FROM node:18.19.1-alpine

RUN apk add git \
	&& apk --no-cache add curl \
	&& apk add jq \
	&& apk update \
	&& apk upgrade \
	&& apk add bash \
	&& apk add openssh
